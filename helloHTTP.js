var http = require('http');
var open = require('open');
var url = require("url");
var vm = require('vm');
var fs = require('fs');
var path = require('path');
var mysql = require('mysql');
var binlog = require('./binlog');
var express = require('express');
var jade = require('jade');
var app = express();

app.set('views', __dirname+"/views");
app.set('view engine', 'jade');

// var sql = req.params.sqli || 'SELECT * from svod_code where type=\'d\' order by code LIMIT 10;';

var config = {
            host: 'ec2-54-208-78-75.compute-1.amazonaws.com',
            user: 'userairs',
            password: 'userairs',
            database : 'airs'
          }

app.get('/', function(req, res){
  res.render('index', {"title": "SERVER V1.0"});
});

app.get('/suicide', function(req, res){
  process.exit();
});

app.get('/xml/:sql', function(req, res){

  renderDATA(req, res, 0);

});

app.get('/json/:sql', function(req, res){

  renderDATA(req, res, 1);

});

function renderDATA (req, res, type) {

  if (type) {
    res.setHeader("Content-Type", "application/json; charset=UTF-8");
  }else{
    res.setHeader("Content-Type", "text/xml; charset=UTF-8");
  };

    var connection = mysql.createConnection(config);
    connection.connect();
    connection.query(req.params.sql, function (err, rows, fields) {
      if (err) throw err;

      if (type) {
        res.send(generateJSON(rows));
      }else{
        body = generateXML(rows);
        res.send(body);
      };

      res.end();
    });
 
    connection.end();
}

function generateXML(rows) {
  var body;
  body = "<ROWSET>";
  for (row in rows) {
    body += "<ROW>";
      for (param in rows[row]) {
        body += "<"+param+">"+JSON.stringify(rows[row][param])+"</"+param+">";
      };
    body += "</ROW>";
  };
  body += "</ROWSET>";
  return body;
}

function generateJSON(rows) {
  return JSON.stringify(rows,2," ");
}

server = app.listen(9000);

open('http://127.0.0.1:9000/');