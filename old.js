var server = http.createServer(function (request, response) {
  
  var pathname = decodeURIComponent(url.parse(request.url).pathname); 

    if(pathname==='/kill/'){
      response.writeHead(200, {"Content-Type": "text/html; charset=UTF-8"});
      console.log("Server killed.");
      response.end("<h1>Server killed</h1>");
      request.connection.destroy();
      process.exit();
    }

    if(pathname==='/xml/'){
      response.writeHead(200, {"Content-Type": "text/xml; charset=UTF-8"});
      var sql = 'SELECT * from svod_code where type=\'d\' order by code LIMIT 100;';
      if(process.argv.length===3) if(process.argv[2]!=='') sql = process.argv[2];
      var connection = mysql.createConnection({
        host: 'ec2-54-208-78-75.compute-1.amazonaws.com',
        user: 'userairs',
        password: 'userairs',
          database : 'airs'
      });
      connection.connect();
      connection.query(sql,
        function(err, rows, fields) {
          if (err) throw err;

              response.write("<ROWSET>");
                for (row in rows) {
                  response.write("<ROW>");
                    for (param in rows[row]) {
                      response.write("<"+param+">"+JSON.stringify(rows[row][param])+"</"+param+">");
                    };
                  response.write("</ROW>");
                };
              response.write("</ROWSET>");

          response.end();
        });
   
      connection.end();
    }

    if(pathname==='/json/'){
      response.writeHead(200, {"Content-Type": "application/json; charset=UTF-8"});
      var sql = 'SELECT * from svod_code where type=\'d\' order by code LIMIT 100;';
      if(process.argv.length===3) if(process.argv[2]!=='') sql = process.argv[2];
      var connection = mysql.createConnection({
        host: 'ec2-54-208-78-75.compute-1.amazonaws.com',
        user: 'userairs',
        password: 'userairs',
          database : 'airs'
      });
      connection.connect();
      connection.query(sql,
        function(err, rows, fields) {
          if (err) throw err;

          response.write(JSON.stringify(rows, "", 4));

          response.end();
        });
   
      connection.end();
    }
});
 
server.listen(9000);
 
console.log("Server running at http://127.0.0.1:9000/");