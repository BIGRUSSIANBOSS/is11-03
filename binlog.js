
function renderDATA (req, res, type) {

  if (type) {
    res.setHeader("Content-Type", "application/json; charset=UTF-8");
  }else{
    res.setHeader("Content-Type", "text/xml; charset=UTF-8");
  };

    var connection = mysql.createConnection(config);
    connection.connect();
    connection.query(req.params.sql, function (err, rows, fields) {
      if (err) throw err;

      if (type) {
        res.send(generateJSON(rows));
      }else{
        body = generateXML(rows);
        res.send(body);
      };

      res.end();
    });
 
    connection.end();
}

function generateXML(rows) {
  var body;
  body = "<ROWSET>";
  for (row in rows) {
    body += "<ROW>";
      for (param in rows[row]) {
        body += "<"+param+">"+JSON.stringify(rows[row][param])+"</"+param+">";
      };
    body += "</ROW>";
  };
  body += "</ROWSET>";
  return body;
}

function generateJSON(rows) {
  return JSON.stringify(rows,2," ");
}